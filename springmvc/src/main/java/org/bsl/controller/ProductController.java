package org.bsl.controller;

import org.bsl.domain.Product;
import org.bsl.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by brian with care.
 */

@Controller
@RequestMapping("/product")
public class ProductController {

    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping({"/", "/list"})
    public String listProducts(Model m) {
        m.addAttribute("products", productService.listAll());
        return "product/list";
    }

    @RequestMapping("/view/{id}")
    public String showProduct(Model m, @PathVariable Integer id) {
        m.addAttribute("product", productService.getById(id));
        return "product/detail";
    }

    @RequestMapping("/edit/{id}")
    public String editProduct(Model m, @PathVariable Integer id) {
        m.addAttribute("product", productService.getById(id));
        return "product/productform";
    }

    @RequestMapping("/new")
    public String newProduct(Model m) {
        m.addAttribute("product", new Product());
        return "product/productform";
    }

    @RequestMapping(method=RequestMethod.POST, value="/save")
    public String saveProduct(@ModelAttribute("product_in") Product p) {
        p = productService.saveOrUpdate(p);
        return "redirect:/product/view/" + p.getId();
    }

    @RequestMapping("/delete/{id}")
    public String deleteProduct(@PathVariable Integer id) {
        productService.delete(id);
        return "redirect:/product/list";
    }
}
