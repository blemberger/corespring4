package org.bsl.services;

import org.bsl.domain.Product;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by brian with care.
 */

@Service
public class ProductServiceImpl extends HashMapCRUDService<Product> implements ProductService {

    protected Map<Integer, Product> loadEntities() {
        Map<Integer, Product> allProducts = new HashMap<>();
        int id;

        Product product = new Product();
        id = getNextId();
        product.setId(id);
        product.setDescription("Product 1");
        product.setPrice(BigDecimal.valueOf(10.99));
        product.setImage("http://www.example/com/product1");
        allProducts.put(id, product);

        product = new Product();
        id = getNextId();
        product.setId(id);
        product.setDescription("Product 2");
        product.setPrice(BigDecimal.valueOf(15.99));
        product.setImage("http://www.example/com/product2");
        allProducts.put(id, product);

        product = new Product();
        id = getNextId();
        product.setId(id);
        product.setDescription("Product 3");
        product.setPrice(BigDecimal.valueOf(20.99));
        product.setImage("http://www.example/com/product3");
        allProducts.put(id, product);

        product = new Product();
        id = getNextId();
        product.setId(id);
        product.setDescription("Product 4");
        product.setPrice(BigDecimal.valueOf(25.99));
        product.setImage("http://www.example/com/product4");
        allProducts.put(id, product);

        product = new Product();
        id = getNextId();
        product.setId(id);
        product.setDescription("Product 5");
        product.setPrice(BigDecimal.valueOf(25.99));
        product.setImage("http://www.example/com/product5");
        allProducts.put(id, product);

        return allProducts;
    }
}
