package org.bsl.services;

import org.bsl.domain.Customer;

/**
 * Created by brian with care.
 */
public interface CustomerService extends CRUDService<Customer> {
}
