package org.bsl.domain;

/**
 * Created by brian with care.
 */
public interface Entity {

    public Integer getId();
    public void setId(Integer id);
}
