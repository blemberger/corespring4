package org.bsl.services;

import org.bsl.domain.Entity;
import org.bsl.domain.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by brian with care.
 */
public abstract class HashMapCRUDService<T extends Entity> implements CRUDService<T> {

    private final Map<Integer, T> entitiesMap = loadEntities();
    private int lastId;

    @Override
    public List<T> listAll() {
        return new ArrayList<T>(entitiesMap.values());
    }

    @Override
    public T getById(Integer id) {
        return entitiesMap.get(id);
    }

    @Override
    public T saveOrUpdate(T entity) {
        if (entity.getId() == null) {
            entity.setId(getNextId());
        }
        entitiesMap.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public void delete(Integer id) {
        entitiesMap.remove(id);
    }

    protected abstract Map<Integer, T> loadEntities();

    synchronized Integer getNextId() {
        return ++lastId;
    }
}
