package org.bsl.controller;

import org.bsl.domain.Customer;
import org.bsl.domain.Product;
import org.bsl.services.CustomerService;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by brian with care.
 */
public class CustomerControllerTest {

    @Mock
    private CustomerService customerService;

    @InjectMocks
    private CustomerController controller;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void testListCustomers() throws Exception {
        List<Customer> testCustomers = new ArrayList<>();
        testCustomers.add(new Customer());
        testCustomers.add(new Customer());
        when(customerService.listAll()).thenReturn(testCustomers);

        mockMvc.perform(get("/customer/list"))
                .andExpect(status().isOk())
                .andExpect(view().name("customer/list"))
                .andExpect(model().attribute("customers", hasSize(2)));
    }

    @Test
    public void testViewCustomer() throws Exception {
        Integer id = 10;
        Customer c = new Customer();
        when(customerService.getById(id)).thenReturn(c);

        mockMvc.perform(get("/customer/view/" + id))
                .andExpect(status().isOk())
                .andExpect(view().name("customer/detail"))
                .andExpect(model().attribute("customer", equalTo(c)));

        mockMvc.perform(get("/customer/view/101"))
                .andExpect(status().isOk())
                .andExpect(view().name("customer/detail"))
                .andExpect(model().attribute("customer", nullValue()));
    }

    @Test
    public void testEditCustomer() throws Exception {
        Integer id = 10;
        Customer c = new Customer();
        when(customerService.getById(id)).thenReturn(c);

        mockMvc.perform(get("/customer/edit/" + id))
                .andExpect(status().isOk())
                .andExpect(view().name("customer/customerform"))
                .andExpect(model().attribute("customer", equalTo(c)));

        mockMvc.perform(get("/customer/edit/101"))
                .andExpect(status().isOk())
                .andExpect(view().name("customer/customerform"))
                .andExpect(model().attribute("customer", nullValue()));
    }

    @Test
    public void testNewCustomer() throws Exception {
        Customer c = new Customer();
        verifyZeroInteractions(customerService);

        mockMvc.perform(get("/customer/new/"))
                .andExpect(status().isOk())
                .andExpect(view().name("customer/customerform"))
                .andExpect(model().attribute("customer", equalTo(c)));
    }

    @Test
    public void testSaveExistingCustomer() throws Exception {
        Integer id = 1;
        String firstName = "Test";
        String lastName = "User";
        String email = "test.user@example.com";
        String phoneNumber = "(312) 555-7611";
        String address1 = "1313 Mockingbird Ln.";
        String address2 = "Unit #0";
        String city = "Berwyn";
        String state = "IL";
        String zipCode = "60060";

        Customer testCustomer = new Customer();
        testCustomer.setId(id);
        testCustomer.setFirstName(firstName);
        testCustomer.setLastName(lastName);
        testCustomer.setEmail(email);
        testCustomer.setPhoneNumber(phoneNumber);
        testCustomer.setAddress1(address1);
        testCustomer.setAddress2(address2);
        testCustomer.setCity(city);
        testCustomer.setState(state);
        testCustomer.setZipCode(zipCode);

        when(customerService.saveOrUpdate(any(Customer.class))).thenReturn(testCustomer);

        mockMvc.perform(post("/customer/save/")
                        .param("id", "1")
                        .param("firstName", firstName)
                        .param("lastName", lastName)
                        .param("email", email)
                        .param("phoneNumber", phoneNumber)
                        .param("address1", address1)
                        .param("address2", address2)
                        .param("city", city)
                        .param("state", state)
                        .param("zipCode", zipCode))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/customer/view/" + id))
                .andExpect(model().attribute("customer", equalTo(testCustomer)));
    }

    @Test
    public void testSaveNewCustomer() throws Exception {
        Integer id = 1;
        String firstName = "Test";
        String lastName = "User";
        String email = "test.user@example.com";
        String phoneNumber = "(312) 555-7611";
        String address1 = "1313 Mockingbird Ln.";
        String address2 = "Unit #0";
        String city = "Berwyn";
        String state = "IL";
        String zipCode = "60060";

        Customer savedCustomer = new Customer();
        savedCustomer.setId(id);
        savedCustomer.setFirstName(firstName);
        savedCustomer.setLastName(lastName);
        savedCustomer.setEmail(email);
        savedCustomer.setPhoneNumber(phoneNumber);
        savedCustomer.setAddress1(address1);
        savedCustomer.setAddress2(address2);
        savedCustomer.setCity(city);
        savedCustomer.setState(state);
        savedCustomer.setZipCode(zipCode);

        when(customerService.saveOrUpdate(any(Customer.class))).thenReturn(savedCustomer);

        mockMvc.perform(post("/customer/save/")
                .param("id", "")  // new customer
                .param("firstName", firstName)
                .param("lastName", lastName)
                .param("email", email)
                .param("phoneNumber", phoneNumber)
                .param("address1", address1)
                .param("address2", address2)
                .param("city", city)
                .param("state", state)
                .param("zipCode", zipCode))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/customer/view/" + id))
                .andExpect(model().attribute("customer", allButIdEqualTo(savedCustomer)));
    }

    @Test
    public void testDeleteCustomer() throws Exception {
        Integer id = 10;

        mockMvc.perform(get("/customer/delete/" + id))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/customer/list"));

        verify(customerService).delete(id);
    }

    @Factory
    private static Matcher<Customer> allButIdEqualTo(final Customer c) {
        return new TypeSafeMatcher<Customer>() {
            @Override
            public boolean matchesSafely(Customer item) {
                return item.getFirstName().equals(c.getFirstName())
                        && item.getLastName().equals(c.getLastName())
                        && item.getEmail().equals(c.getEmail())
                        && item.getPhoneNumber().equals(c.getPhoneNumber())
                        && item.getAddress1().equals(c.getAddress1())
                        && item.getAddress2().equals(c.getAddress2())
                        && item.getCity().equals(c.getCity())
                        && item.getState().equals(c.getState())
                        && item.getZipCode().equals(c.getZipCode());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("all Customer fields the same except (possibly) for Id");
            }
        };
    }
}