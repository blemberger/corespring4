package org.bsl.controllers;

import org.bsl.services.HelloWorldService;

/**
 * Created by brian on 2/25/16.
 */
public class GreetingController {

    private HelloWorldService helloWorldService;
    private HelloWorldService it_helloWorldService;
    private HelloWorldService ru_helloWorldService;
    private HelloWorldService de_helloWorldService;

    public void setHelloWorldService(HelloWorldService helloWorldService) {
        this.helloWorldService = helloWorldService;
    }

    public void setItalianHelloWorldService(HelloWorldService hws) {
        this.it_helloWorldService = hws;
    }

    public void setRussianHelloWorldService(HelloWorldService hws) {
        this.ru_helloWorldService = hws;
    }

    public void setGermanHelloWorldService(HelloWorldService hws) {
        this.de_helloWorldService = hws;
    }

    public String sayHello() {
        String greeting = helloWorldService.getGreeting();
        System.out.println(greeting);

        String it_greeting = it_helloWorldService.getGreeting();
        System.out.println(it_greeting);

        String ru_greeting = ru_helloWorldService.getGreeting();
        System.out.println(ru_greeting);

        String de_greeting = de_helloWorldService.getGreeting();
        System.out.println(de_greeting);
        return greeting;
    }
}
