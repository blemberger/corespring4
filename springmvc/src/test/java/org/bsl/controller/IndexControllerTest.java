package org.bsl.controller;

import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by brian with care.
 */
public class IndexControllerTest {

    private IndexController controller = new IndexController();
    private MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller).build();

    @Test
    public void testIndex() throws Exception {
        ResultActions actions = mockMvc.perform(get("/"))
                                        .andExpect(status().isOk())
                                        .andExpect(view().name("index"));
    }
}