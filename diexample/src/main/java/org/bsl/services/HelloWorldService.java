package org.bsl.services;

/**
 * Created by brian on 2/25/16.
 */
public interface HelloWorldService {
    String getGreeting();
}
