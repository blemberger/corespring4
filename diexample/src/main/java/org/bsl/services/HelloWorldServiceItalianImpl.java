package org.bsl.services;

/**
 * Created by brian on 2/25/16.
 */

public class HelloWorldServiceItalianImpl implements HelloWorldService {
    @Override
    public String getGreeting() {
        return "Ciao mondo!";
    }
}
