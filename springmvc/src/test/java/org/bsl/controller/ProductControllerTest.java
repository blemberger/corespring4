package org.bsl.controller;

import org.bsl.domain.Product;
import org.bsl.services.ProductService;
import org.hamcrest.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;

/**
 * Created by brian with care.
 */
public class ProductControllerTest {

    @Mock
    private ProductService productService;

    @InjectMocks
    private ProductController controller;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void testListProducts() throws Exception {
        List<Product> testProducts = new ArrayList<>();
        testProducts.add(new Product());
        testProducts.add(new Product());
        when(productService.listAll()).thenReturn(testProducts);

        mockMvc.perform(get("/product/list"))
                .andExpect(status().isOk())
                .andExpect(view().name("product/list"))
                .andExpect(model().attribute("products", hasSize(2)));
    }

    @Test
    public void testViewProduct() throws Exception {
        Integer id = 10;
        Product p = new Product();
        when(productService.getById(id)).thenReturn(p);

        mockMvc.perform(get("/product/view/" + id))
                .andExpect(status().isOk())
                .andExpect(view().name("product/detail"))
                .andExpect(model().attribute("product", equalTo(p)));

        mockMvc.perform(get("/product/view/101"))
                .andExpect(status().isOk()) // actually, the DispatcherServlet returns status 500 because of thymeleaf
                                            // binding error
                .andExpect(view().name("product/detail"))
                .andExpect(model().attribute("product", nullValue()));
    }

    @Test
    public void testEditProduct() throws Exception {
        Integer id = 10;
        Product p = new Product();
        when(productService.getById(id)).thenReturn(p);

        mockMvc.perform(get("/product/edit/" + id))
                .andExpect(status().isOk())
                .andExpect(view().name("product/productform"))
                .andExpect(model().attribute("product", equalTo(p)));

        mockMvc.perform(get("/product/edit/101"))
                .andExpect(status().isOk())
                .andExpect(view().name("product/productform"))
                .andExpect(model().attribute("product", nullValue()));
    }

    @Test
    public void testNewProduct() throws Exception {
        Product p = new Product();
        verifyZeroInteractions(productService);

        mockMvc.perform(get("/product/new/"))
                .andExpect(status().isOk())
                .andExpect(view().name("product/productform"))
                .andExpect(model().attribute("product", equalTo(p)));
    }

    @Test
    public void testSaveExistingProduct() throws Exception {
        Integer id = 1;
        String description = "Test Product";
        String price = "10.99";
        String imageUrl = "http://example.com/test";

        Product testProduct = new Product();
        testProduct.setId(id);
        testProduct.setDescription(description);
        testProduct.setPrice(new BigDecimal(price));
        testProduct.setImage(imageUrl);

        when(productService.saveOrUpdate(any(Product.class))).thenReturn(testProduct);

        mockMvc.perform(post("/product/save/")
                        .param("id", "1")
                        .param("description", description)
                        .param("price", price)
                        .param("image", imageUrl))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/product/view/" + id))
                .andExpect(model().attribute("product_in", equalTo(testProduct)));  // "save_product" is added to the model
                                                                                    // automatically by ProductController.saveProduct().
                                                                                    // It uses the name set in the optional
                                                                                    // @ModelAttribute annotation.
    }

    @Test
    public void testSaveNewProduct() throws Exception {
        Integer id = 1;
        String description = "Test Product";
        String price = "10.99";
        String imageUrl = "http://example.com/test";

        Product savedProduct = new Product();
        savedProduct.setId(id);
        savedProduct.setDescription(description);
        savedProduct.setPrice(new BigDecimal(price));
        savedProduct.setImage(imageUrl);

        when(productService.saveOrUpdate(any(Product.class))).thenReturn(savedProduct);

        mockMvc.perform(post("/product/save/")
                .param("id", "") // new product
                .param("description", description)
                .param("price", price)
                .param("image", imageUrl))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/product/view/" + id))
                .andExpect(model().attribute("product_in", allButIdEqualTo(savedProduct)));
    }

    @Test
    public void testDeleteProduct() throws Exception {
        Integer id = 10;

        mockMvc.perform(get("/product/delete/" + id))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/product/list"));

        verify(productService).delete(id);
    }

    @Factory
    private static Matcher<Product> allButIdEqualTo(final Product p) {
        return new TypeSafeMatcher<Product>() {
            @Override
            public boolean matchesSafely(Product item) {
                return item.getDescription().equals(p.getDescription())
                        && item.getPrice().equals(p.getPrice())
                        && item.getImage().equals(p.getImage());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("all Product fields the same except (possibly) for Id");
            }
        };
    }
}