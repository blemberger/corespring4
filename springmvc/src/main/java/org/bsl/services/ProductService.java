package org.bsl.services;

import org.bsl.domain.Product;

/**
 * Created by brian with care.
 */
public interface ProductService extends CRUDService<Product> {
}
