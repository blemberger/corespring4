package org.bsl.services;

/**
 * Created by brian with care.
 */
public class HelloWorldServiceFactory {

    public HelloWorldService helloWorldService(String lang) {
        switch (lang) {
            case "en":  return new HelloWorldServiceEnglishImpl();
            case "es":  return new HelloWorldServiceSpanishImpl();
            case "de":  return new HelloWorldServiceGermanImpl();
            case "ru":  return new HelloWorldServiceRussianImpl();
            case "it":  return new HelloWorldServiceItalianImpl();
            default:    return null; // No good
        }
    }
}
