package org.bsl.services;

import org.bsl.domain.Customer;
import org.bsl.domain.Entity;

import java.util.List;

/**
 * Created by brian with care.
 */
public interface CRUDService<T extends Entity> {

    List<T> listAll();

    T getById(Integer id);

    T saveOrUpdate(T t);

    void delete(Integer id);
}
