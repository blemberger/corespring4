package org.bsl.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * Created by brian with care.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/helloworld-config.xml",
        "classpath:spring/en-helloworld-config.xml"})
public class HelloWorldServiceEnglishImplTest {

    @Autowired
    private HelloWorldService service;

    @Test
    public void testGetGreeting() throws Exception {
        // Obtain the test result
        String greeting = service.getGreeting();

        // Make assertions about the test result
        Assert.assertEquals("The greeting returned was unexpected", "Hello World!", greeting);
    }
}