package org.bsl.controller;

import org.bsl.domain.Customer;
import org.bsl.domain.Product;
import org.bsl.services.CustomerService;
import org.bsl.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by brian with care.
 */

@Controller
@RequestMapping("/customer")
public class CustomerController {

    private CustomerService customerService;

    @Autowired
    public void setCustomerService(CustomerService service) {
        this.customerService = service;
    }

    @RequestMapping({"/", "/list"})
    public String listProducts(Model m) {
        m.addAttribute("customers", customerService.listAll());
        return "customer/list";
    }

    @RequestMapping("/view/{id}")
    public String showCustomer(Model m, @PathVariable Integer id) {
        m.addAttribute("customer", customerService.getById(id));
        return "customer/detail";
    }

    @RequestMapping("/edit/{id}")
    public String editCustomer(Model m, @PathVariable Integer id) {
        m.addAttribute("customer", customerService.getById(id));
        return "customer/customerform";
    }

    @RequestMapping("/new")
    public String newCustomer(Model m) {
        m.addAttribute("customer", new Customer());
        return "customer/customerform";
    }

    @RequestMapping(method=RequestMethod.POST, value="/save")
    public String saveCustomer(Customer c) {
        c = customerService.saveOrUpdate(c);
        return "redirect:/customer/view/" + c.getId();
    }

    @RequestMapping("/delete/{id}")
    public String deleteCustomer(@PathVariable Integer id) {
        customerService.delete(id);
        return "redirect:/customer/list";
    }
}
