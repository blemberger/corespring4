package org.bsl.services;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by brian with care.
 */
public class ProductServiceImplTest {

    ProductServiceImpl service = new ProductServiceImpl();

    @Test
    public void testGetNextId() throws Exception {
        int currentProductSize = service.listAll().size();
        assertEquals(new Integer(currentProductSize + 1), service.getNextId());
    }

}