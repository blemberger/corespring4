package org.bsl.services;

import org.bsl.domain.Customer;
import org.bsl.domain.Product;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by brian with care.
 */

@Service
public class CustomerServiceImpl extends HashMapCRUDService<Customer> implements CustomerService {

    @Override
    protected Map<Integer, Customer> loadEntities() {
        Map<Integer, Customer> customerMap = new HashMap<>();

        Customer c = new Customer();
        c.setId(getNextId());
        c.setLastName("Lemberger");
        c.setFirstName("Brian");
        c.setEmail("brian.lemberger@gmail.com");
        c.setPhoneNumber("708-738-8285");
        c.setAddress1("109 N. Emerson St.");
        c.setAddress2("");
        c.setCity("Mount Prospect");
        c.setState("IL");
        c.setZipCode("60056");
        customerMap.put(c.getId(), c);

        c = new Customer();
        c.setId(getNextId());
        c.setLastName("Lemberger");
        c.setFirstName("Nicholas");
        c.setEmail("");
        c.setPhoneNumber("847-749-2866");
        c.setAddress1("109 N. Emerson St.");
        c.setAddress2("");
        c.setCity("Mount Prospect");
        c.setState("IL");
        c.setZipCode("60056");
        customerMap.put(c.getId(), c);

        c = new Customer();
        c.setId(getNextId());
        c.setLastName("Zervas");
        c.setFirstName("Mary Jo");
        c.setEmail("maryjoz@gmail.com");
        c.setPhoneNumber("630-319-7635");
        c.setAddress1("109 N. Emerson St.");
        c.setAddress2("");
        c.setCity("Mount Prospect");
        c.setState("IL");
        c.setZipCode("60056");
        customerMap.put(c.getId(), c);

        return customerMap;
    }
}
